﻿namespace BWSAPIExtension.Models
{
    public class GetSecretRequest
    {
        public string APIToken { get; set; }
        public string User { get; set; }
        public string Type { get; set; }
        public string ProjectId { get; set; }
    }
}
