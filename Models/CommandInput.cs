﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BWSAPIExtension.Models
{
    public class CommandInput
    {
        public string AccessToken { get; set; }
        public string OrganizationId { get; set; }
        public string Key { get; set; }
    }
}
