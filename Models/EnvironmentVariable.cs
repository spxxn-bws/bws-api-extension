﻿using System.Text.Json.Serialization;

namespace BWSAPIExtension.Models
{
    public class EnvironmentVariable
    {
        [JsonPropertyName("accessList")]
        public List<AccessList> AccessList { get; set; }

    }
    public class AccessList
    {
        [JsonPropertyName("User")]
        public string User { get; set; }

        [JsonPropertyName("apiToken")]
        public string ApiToken { get; set; }

        [JsonPropertyName("AccessTokens")]
        public List<AccessTokens> AccessTokens { get; set; }
    }
    public class AccessTokens
    {
        [JsonPropertyName("AccessToken")]
        public string AccessToken { get; set; }

        [JsonPropertyName("OrganizationIds")]
        public List<OriganizationIds> OrganizationIds { get; set; }
    }
    public class OriganizationIds
    {
        [JsonPropertyName("OrganizationId")]
        public string OrganizationId { get; set; }
    }
}
