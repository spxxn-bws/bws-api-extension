#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM registry.gitlab.com/spxxn-bws/bws-api-extension/bwsubuntu:latest AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
#RUN apt update
#RUN apt install -y wget
#RUN apt install -y unzip
#RUN wget https://gitlab.com/spxxn-bws/bws-scripts/-/blob/main/bwsInstallScript.sh
#RUN chmod +x bwsInstallScript.sh
#RUN ./bwsInstallScript.sh

FROM registry.gitlab.com/spxxn-bws/bws-api-extension/bwsubuntu:latest AS build
#RUN apt update
#RUN apt install -y wget
#RUN apt install -y unzip
#RUN wget https://offsite-backup.us-southeast-1.linodeobjects.com/Scripting/BWS/bwsInstallScript.sh
#RUN chmod +x ./bwsInstallScript.sh
#RUN ./bwsInstallScript.sh
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["BWSAPIExtension.csproj", "."]
RUN dotnet restore "./BWSAPIExtension.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "./BWSAPIExtension.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./BWSAPIExtension.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "BWSAPIExtension.dll"]