﻿using BWSAPIExtension.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BWSAPIExtension.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BWSController : ControllerBase
    {
        [HttpPost]
        [Route("getSecrets")]
        public IActionResult GetSecrets(GetSecretRequest request)
        {
            try
            {
                var setupBWS = new BWS();
                bool validated = setupBWS.ValidateAPI(request);
                //string apiToken = Environment.GetEnvironmentVariable("API_TOKEN")!;
                if (validated)
                {
                    List<BWSResponse> response = setupBWS.GetSecret(request);
                    return Ok(response);
                }
                else
                {
                    return BadRequest("Fuck off");
                }
            }
            catch(Exception ex)
            {
                return BadRequest("Exception generated");
            }
        }
    }
}
