﻿using System.Diagnostics;
using Newtonsoft.Json;
using BWSAPIExtension.Models;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

namespace BWSAPIExtension
{
    public class BWS
    {
        public List<BWSResponse> GetSecret(GetSecretRequest request)
        {
            try
            {
                string bwsAccessInfo = Environment.GetEnvironmentVariable("BWS_ACCESS_INFO")!;
                EnvironmentVariable bwsAccessInfoModel = JsonConvert.DeserializeObject<EnvironmentVariable>(bwsAccessInfo);
                AccessList accessList = bwsAccessInfoModel.AccessList.Where(x => x.User == request.User).FirstOrDefault();
                List<BWSResponse> mainList = new List<BWSResponse>();
                //string bwToken = Environment.GetEnvironmentVariable("BWS_ACCESS_TOKEN")!;
                //var orgId = Environment.GetEnvironmentVariable("BWS_ORG_ID")!;
                //var orgId = Guid.Parse(orgIdString);
                foreach(var x in accessList.AccessTokens)
                {
                    foreach(var y in x.OrganizationIds)
                    {
                        var input = new CommandInput { AccessToken = x.AccessToken, OrganizationId = y.OrganizationId, Key = request.Type };
                        var response = RunCommandWithBatch(input);
                        List<BWSResponse> fullResponse = JsonConvert.DeserializeObject<List<BWSResponse>>(response);
                        mainList.AddRange(fullResponse);
                    }
                }
                if (request.Type == "all")
                {
                    return mainList;
                }
                else
                {
                    List<string> orgCheck = new List<string>();
                    foreach(var x in accessList.AccessTokens)
                    {
                        foreach(var y in x.OrganizationIds)
                        {
                            orgCheck.Add(y.OrganizationId);
                        }
                    }
                    List<BWSResponse> getInfo = new List<BWSResponse>();
                    if(request.ProjectId != "")
                    {
                        getInfo = mainList.Where(x => orgCheck.Contains(x.OrganizationId))
                            .Where(x => x.ProjectId == request.ProjectId).ToList();
                    }
                    else
                    {
                        getInfo = mainList.Where(x => x.Key == request.Type).Where(x => orgCheck.Contains(x.OrganizationId)).ToList();
                    }

                    return getInfo;
                }
            }
            catch (Exception ex)
            {
                string docPath = "";
                if(RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    docPath = "C:/log/";
                }
                else
                {
                    docPath = "/var/log/";
                }
                using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "bwsm.log")))
                {
                    outputFile.WriteLine(ex);
                }
                List<BWSResponse> failure = new List<BWSResponse>();
                failure.Add(new BWSResponse()
                {
                    Id = "Failure",
                    OrganizationId = "Failure",
                    ProjectId = "Failure",
                    Key = "Failure",
                    Value = "Failure",
                    Note = "Failure",
                    CreationDate = "Failure",
                    RevisionDate = "Failure"
                });
                return failure;
            }
        }
        public string RunCommandWithBatch(CommandInput input)
        {
            var psi = new ProcessStartInfo();
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                psi.WorkingDirectory = "/bin";
            }
            psi.FileName = "bws";
            psi.Arguments = $" secret list -t {input.AccessToken}";
            psi.RedirectStandardOutput = true;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;

            using var process = Process.Start(psi);
            process.WaitForExit();
            var output = process.StandardOutput.ReadToEnd();

            return output;
        }
        public bool ValidateAPI(GetSecretRequest request)
        {
            bool validated = false;
            string bwsAccessInfo = Environment.GetEnvironmentVariable("BWS_ACCESS_INFO")!;
            EnvironmentVariable bwsAccessInfoModel = JsonConvert.DeserializeObject<EnvironmentVariable>(bwsAccessInfo);
            AccessList user = bwsAccessInfoModel.AccessList.Where(x => x.User ==  request.User).FirstOrDefault();
            if(user.ApiToken == request.APIToken)
            {
                validated = true;
            }
            return validated;
        }
    }
}
